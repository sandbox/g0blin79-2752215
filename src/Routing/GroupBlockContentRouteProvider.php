<?php

/**
 * @file
 * Contains \Drupal\gblock_content\Routing\GroupBlockContentRouteProvider.
 */

namespace Drupal\gblock_content\Routing;

use Drupal\block_content\Entity\BlockContentType;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_block_content group content.
 */
class GroupBlockContentRouteProvider {

  /**
   * Provides the shared collection route for group block_content plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (BlockContentType::loadMultiple() as $name => $content) {
      $plugin_id = "group_block_content:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id block_content";
      $permissions_create[] = "create $name block_content";
    }

    // If there are no block_content types yet, we cannot have any plugin IDs and should
    // therefore exit early because we cannot have any routes for them either.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_block_content.collection'] = new Route('group/{group}/block_content');
    $routes['entity.group_content.group_block_content.collection']
      ->setDefaults([
        '_entity_list' => 'group_content',
        '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
        'plugin_id' => $plugin_ids,
      ])
      ->setRequirement('_group_permission', 'access group_block_content overview')
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE)
      ->setOption('parameters', [
        'group' => ['type' => 'entity:group'],
      ]);

    $routes['entity.group_content.group_block_content.add_page'] = new Route('group/{group}/block_content/add');
    $routes['entity.group_content.group_block_content.add_page']
      ->setDefaults([
        '_title' => 'Add Block content',
        '_controller' => '\Drupal\gblock_content\Controller\GroupBlockContentController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_block_content.create_page'] = new Route('group/{group}/block_content/create');
    $routes['entity.group_content.group_block_content.create_page']
      ->setDefaults([
        '_title' => 'Create Block content',
        '_controller' => '\Drupal\gblock_content\Controller\GroupBlockContentController::createPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
