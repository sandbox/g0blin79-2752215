<?php

/**
 * @file
 * Contains \Drupal\gblock_content\Controller\GroupBlockContentController.
 */

namespace Drupal\gblock_content\Controller;

use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\block_content\BlockContentTypeInterface;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides group block_content route controllers.
 *
 * This only controls the routes that are not supported out of the box by the
 * plugin base \Drupal\group\Plugin\GroupContentEnablerBase.
 */
class GroupBlockContentController extends ControllerBase {

  /**
   * The private store for temporary group block_contents.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new GroupBlockContentController.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, AccountInterface $current_user, RequestStack $request_stack) {
    $this->privateTempStore = $temp_store_factory->get('gblock_content_add_temp');
    $this->currentUser = $current_user;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('current_user'),
      $container->get('request_stack')
    );
  }

  /**
   * Provides the form for creating a block_content in a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to create a block_content in.
   * @param \Drupal\block_content\BlockContentTypeInterface $block_content_type
   *   The block_content type to create.
   *
   * @return array
   *   The form array for either step 1 or 2 of the group block_content creation wizard.
   */
  public function add(GroupInterface $group, BlockContentTypeInterface $block_content_type) {
    $plugin_id = 'group_block_content:' . $block_content_type->id();
    $storage_id = $plugin_id . ':' . $group->id();

    // If we are on step one, we need to build a block_content form.
    if ($this->privateTempStore->get("$storage_id:step") !== 2) {
      $this->privateTempStore->set("$storage_id:step", 1);

      // Only create a new block_content if we have nothing stored.
      if (!$entity = $this->privateTempStore->get("$storage_id:block_content")) {
        $entity = BlockContent::create(['type' => $block_content_type->id()]);
      }
    }
    // If we are on step two, we need to build a group content form.
    else {
      /** @var \Drupal\group\Plugin\GroupContentEnablerInterface $plugin */
      $plugin = $group->getGroupType()->getContentPlugin($plugin_id);
      $entity = GroupContent::create([
        'type' => $plugin->getContentTypeConfigId(),
        'gid' => $group->id(),
      ]);
    }

    // Return the form with the group and storage ID added to the form state.
    $extra = ['group' => $group, 'storage_id' => $storage_id];
    return $this->entityFormBuilder()->getForm($entity, 'gblock_content-form', $extra);
  }

  /**
   * The _title_callback for the add block_content form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to create a block_content in.
   * @param \Drupal\block_content\BlockContentTypeInterface $block_content_type
   *   The block_content type to create.
   *
   * @return string
   *   The page title.
   */
  public function addTitle(GroupInterface $group, BlockContentTypeInterface $block_content_type) {
    return $this->t('Create %type in %label', ['%type' => $block_content_type->label(), '%label' => $group->label()]);
  }

  /**
   * Displays add block_content links for available group block_content types.
   *
   * Redirects to group/{group}/block_content/add/{block_content_type} if only one group block_content
   * type is available.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to add a block_content to.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A render array for a list of the block content types that can be added. However,
   *   if there is only one type available to the user, the function will
   *   return a RedirectResponse to the block_content add page for that type.
   */
  public function addPage(GroupInterface $group) {
    $plugins = $group->getGroupType()->getInstalledContentPlugins();

    $block_content_type_ids = [];
    foreach ($plugins as $plugin) {
      /** @var \Drupal\group\Plugin\GroupContentEnablerInterface $plugin */
      list($base_plugin_id, $derivative_id) = explode(':', $plugin->getPluginId() . ':');

      // Only show the block_content types the user has access to.
      if ($base_plugin_id == 'group_block_content' && $plugin->createAccess($group, $this->currentUser)) {
        $block_content_type_ids[] = $derivative_id;
      }
    }

    // Bypass the page if only one block_content type is available.
    if (count($block_content_type_ids) == 1) {
      $block_content_type_id = reset($block_content_type_ids);
      $plugin = $group->getGroupType()->getContentPlugin("group_block_content:$block_content_type_id");
      return $this->redirect($plugin->getRouteName('add-form'), ['group' => $group->id()]);
    }

    return [
      '#theme' => 'gblock_content_add_list',
      '#group' => $group,
      '#block_content_types' => BlockContentType::loadMultiple($block_content_type_ids),
    ];
  }

  /**
   * Displays create block_content links for available group types.
   *
   * Redirects to group/{group}/block_content/create/{block_content_type} if only one group type
   * is available.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to create a block_content in.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A render array for a list of the block_contents that can be created. However,
   *   if there is only one block_content type available to the user, the function will
   *   return a RedirectResponse to the block_content create page for that type.
   */
  public function createPage(GroupInterface $group) {
    $plugins = $group->getGroupType()->getInstalledContentPlugins();

    $block_content_type_ids = [];
    foreach ($plugins as $plugin) {
      /** @var \Drupal\group\Plugin\GroupContentEnablerInterface $plugin */
      list($base_plugin_id, $derivative_id) = explode(':', $plugin->getPluginId() . ':');

      // Only show the block_content types the user has access to.
      if ($base_plugin_id == 'group_block_content' && $group->hasPermission("create $derivative_id block_content", $this->currentUser)) {
        $block_content_type_ids[] = $derivative_id;
      }
    }

    // Bypass the page if only one content type is available.
    if (count($block_content_type_ids) == 1) {
      $block_content_type_id = reset($block_content_type_ids);
      $plugin = $group->getGroupType()->getContentPlugin("group_block_content:$block_content_type_id");
      return $this->redirect($plugin->getRouteName('create-form'), ['group' => $group->id()]);
    }

    return [
      '#theme' => 'gblock_content_create_list',
      '#group' => $group,
      '#block_content_types' => BlockContentType::loadMultiple($block_content_type_ids),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Overwritten to pass on the URL redirect parameter instead of following it.
   */
  protected function redirect($route_name, array $route_parameters = [], array $options = [], $status = 302) {
    $options['query'] = $this->currentRequest->query->all();
    $this->currentRequest->query->remove('destination');
    return parent::redirect($route_name, $route_parameters, $options, $status);
  }
}
