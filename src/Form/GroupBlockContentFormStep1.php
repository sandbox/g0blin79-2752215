<?php

/**
 * @file
 * Contains \Drupal\gblock_content\Form\GroupBlockContentFormStep1.
 */

namespace Drupal\gblock_content\Form;

use Drupal\block_content\BlockContentForm;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a creating a block_content without it being saved yet.
 */
class GroupBlockContentFormStep1 extends BlockContentForm {

  /**
   * The private store for temporary group block_content.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Constructs a GroupBlockContentFormStep1 object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $block_content_storage
   *   The custom block storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $block_content_type_storage
   *   The custom block type storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   */
  public function __construct(
    EntityManagerInterface $entity_manager,
    EntityStorageInterface $block_content_storage,
    EntityStorageInterface $block_content_type_storage,
    LanguageManagerInterface $language_manager,
    PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($entity_manager, $block_content_storage, $block_content_type_storage, $language_manager);
    $this->privateTempStore = $temp_store_factory->get('gblock_content_add_temp');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager,
      $entity_manager->getStorage('block_content'),
      $entity_manager->getStorage('block_content_type'),
      $container->get('language_manager'),
      $container->get('user.private_tempstore')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue to final step'),
      '#submit' => ['::submitForm', '::saveTemporary'],
    ];

    $actions['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancel'],
      '#limit_validation_errors' => [],
    ];

    return $actions;
  }

  /**
   * Saves a temporary block_content and continues to step 2 of group block_content creation.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\gblock_content\Controller\GroupBlockContentController::add()
   * @see \Drupal\gblock_content\Form\GroupBlockContentStep2
   */
  public function saveTemporary(array &$form, FormStateInterface $form_state) {
    $storage_id = $form_state->get('storage_id');

    $this->privateTempStore->set("$storage_id:block_content", $this->entity);
    $this->privateTempStore->set("$storage_id:step", 2);

    // Disable any URL-based redirect until the final step.
    $request = $this->getRequest();
    $form_state->setRedirectUrl(Url::fromRoute('<current>', [], ['query' => $request->query->all()]));
    $request->query->remove('destination');
  }

  /**
   * Cancels the block_content creation by emptying the temp store.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\gblock_content\Controller\GroupBlockContentController::add()
   */
  public function cancel(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $form_state->get('group');

    $storage_id = $form_state->get('storage_id');
    $this->privateTempStore->delete("$storage_id:block_content");

    // Redirect to the collection page if no destination was set in the URL.
    $plugin = $group->getGroupType()->getContentPlugin('group_block_content:' . $this->entity->bundle());
    $form_state->setRedirect($plugin->getRouteName('collection'), ['group' => $group->id()]);
  }

}
