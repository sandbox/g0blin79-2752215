<?php

/**
 * @file
 * Contains \Drupal\gblock_content\Plugin\GroupContentEnabler\GroupBlockContent.
 */

namespace Drupal\gblock_content\Plugin\GroupContentEnabler;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides a content enabler for block_content.
 *
 * @GroupContentEnabler(
 *   id = "group_block_content",
 *   label = @Translation("Group block content"),
 *   description = @Translation("Adds block contents to groups both publicly and privately."),
 *   entity_type_id = "block_content",
 *   path_key = "block_content",
 *   deriver = "Drupal\gblock_content\Plugin\GroupContentEnabler\GroupBlockContentDeriver"
 * )
 */
class GroupBlockContent extends GroupContentEnablerBase {

  /**
   * Retrieves the block content types this plugin supports.
   *
   * @return \Drupal\block_content\BlockContentTypeInterface
   *   The block_content type this plugin supports.
   */
  protected function getBlockContentType() {
    return BlockContentType::load($this->getEntityBundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $account = \Drupal::currentUser();
    $type = $this->getEntityBundle();
    $operations = [];

    if ($group->hasPermission("create $type block_content", $account)) {
      $operations["gblock_content-create-$type"] = [
        'title' => $this->t('Create @type', ['@type' => $this->getBlockContentType()->label()]),
        'url' => new Url($this->getRouteName('create-form'), ['group' => $group->id()]),
        'weight' => 30,
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityForms() {
    return ['gblock_content-form' => 'Drupal\gblock_content\Form\GroupBlockContentFormStep2'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $permissions = parent::getPermissions();

    // Unset unwanted permissions defined by the base plugin.
    $plugin_id = $this->getPluginId();
    unset($permissions["access $plugin_id overview"]);

    // Add our own permissions for managing the actual block_content.
    $type = $this->getEntityBundle();
    $type_arg = ['%block_content_type' => $this->getBlockContentType()->label()];
    $defaults = [
      'title_args' => $type_arg,
      'description' => 'Only applies to %block_content_type block_content that belong to this group.',
      'description_args' => $type_arg,
    ];

    $permissions["view $type block_content"] = [
      'title' => '%block_content_type: View block content',
    ] + $defaults;

    $permissions["create $type block_content"] = [
      'title' => '%block_content_type: Create new block content',
      'description' => 'Allows you to create %block_content_type block_content that immediately belong to this group.',
      'description_args' => $type_arg,
    ] + $defaults;

    $permissions["edit own $type block_content"] = [
      'title' => '%block_content_type: Edit own block content',
    ] + $defaults;

    $permissions["edit any $type block_content"] = [
      'title' => '%block_content_type: Edit any block content',
    ] + $defaults;

    $permissions["delete own $type block_content"] = [
      'title' => '%block_content_type: Delete own block content',
    ] + $defaults;

    $permissions["delete any $type block_content"] = [
      'title' => '%block_content_type: Delete any block content',
    ] + $defaults;

    return $permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaths() {
    $paths = parent::getPaths();

    $type = $this->getEntityBundle();
    $paths['add-form'] = "/group/{group}/block_content/add/$type";
    $paths['create-form'] = "/group/{group}/block_content/create/$type";

    return $paths;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\gblock_content\Routing\GroupBlockContentRouteProvider
   */
  public function getRouteName($name) {
    if ($name == 'collection') {
      return 'entity.group_content.group_block_content.collection';
    }
    return parent::getRouteName($name);
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\gblock_content\Routing\GroupBlockContentRouteProvider
   */
  protected function getCollectionRoute() {
  }

  /**
   * Gets the create form route.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCreateFormRoute() {
    if ($path = $this->getPath('create-form')) {
      $route = new Route($path);

      $route
        ->setDefaults([
          '_controller' => '\Drupal\gblock_content\Controller\GroupBlockContentController::add',
          '_title_callback' => '\Drupal\gblock_content\Controller\GroupBlockContentController::addTitle',
          'block_content_type' => $this->getEntityBundle(),
        ])
        ->setRequirement('_group_permission', 'create ' . $this->getEntityBundle() . ' block_content')
        ->setRequirement('_group_installed_content', $this->getPluginId())
        ->setOption('_group_operation_route', TRUE)
        ->setOption('parameters', [
          'group' => ['type' => 'entity:group'],
        ]);

      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes() {
    $routes = parent::getRoutes();

    if ($route = $this->getCreateFormRoute()) {
      $routes[$this->getRouteName('create-form')] = $route;
    }

    return $routes;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\gblock_content\Routing\GroupBlockContentRouteProvider
   */
  public function getLocalActions() {
    $actions['group_block_content.add'] = [
      'title' => 'Add block content',
      'route_name' => 'entity.group_content.group_block_content.add_page',
      'appears_on' => [$this->getRouteName('collection')],
    ];

    $actions['group_block_content.create'] = [
      'title' => 'Create block content',
      'route_name' => 'entity.group_content.group_block_content.create_page',
      'appears_on' => [$this->getRouteName('collection')],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other content enabler plugins.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    return $form;
  }

}
