<?php

/**
 * @file
 * Contains \Drupal\gblock_content\Plugin\GroupContentEnabler\GroupBlockContentDeriver.
 */

namespace Drupal\gblock_content\Plugin\GroupContentEnabler;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupBlockContentDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (BlockContentType::loadMultiple() as $name => $content) {
      $label = $content->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group block content') . " ($label)",
        'description' => t('Adds %type block content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
